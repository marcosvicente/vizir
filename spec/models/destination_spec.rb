require 'rails_helper'

RSpec.describe Destination, type: :model do
  describe 'with validations' do
    it { is_expected.to validate_presence_of(:dd) }
  end

  describe 'create a new destination' do
    context 'when create a destination' do
      it 'valid arguments' do
        create(:orign)
        expect(Orign.count).to eq(1)
      end
    end
  end
end
