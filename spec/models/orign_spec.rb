require 'rails_helper'

RSpec.describe Orign, type: :model do
  describe 'with validations' do
    it { is_expected.to validate_presence_of(:dd) }
  end

  describe 'create a new orign' do
    context 'when create a orign' do
      it 'valid arguments' do
        create(:orign)
        expect(Orign.count).to eq(1)
      end
    end
  end
end
