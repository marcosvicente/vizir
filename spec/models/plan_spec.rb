require 'rails_helper'

RSpec.describe Plan, type: :model do

  describe "with validations" do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:minutes) }
  end

  describe "create a new plan" do
    context "when create a Plan" do
      it "valid arguments" do
        create(:plan)
        expect(Plan.count).to eq(1)
      end
    end

  end
end
