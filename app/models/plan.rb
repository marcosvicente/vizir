class Plan < ApplicationRecord
  validates :name, presence: true
  validates :minutes, presence: true

end
