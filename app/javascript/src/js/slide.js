class Slide{
  constructor() {
    this.slideIndex = 1;
  }
  /*
  * show the first slide
  */
  showFirst(){
    this.showSlides(this.slideIndex);
  }
  /**
   * Next/previous controls
   */
  plusSlides(n) {
    this.showSlides(this.slideIndex += n);
  }

  /**
   * Thumbnail image controls
   */
  currentSlide(n) {
    this.showSlides(this.slideIndex = n);
  }

  /**
   *  showSlides
   */
  showSlides(n){
    var i;
    var slides = document.getElementsByClassName('slides');
    var dots = document.getElementsByClassName('dot');
    if (n > slides.length) {
      this.slideIndex = 1
    }

    if (n < 1) {
      this.slideIndex = slides.length
    }

    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = 'none';
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace('active', '');
    }

    slides[this.slideIndex - 1].style.display = 'block';
    dots[this.slideIndex - 1].className += 'active';
  }

}

var slide = new Slide();
// function for run with the turbolinks
slide.showFirst();

document.getElementById('prev').onclick = () =>  slide.plusSlides(-1) ;
document.getElementById('next').onclick = () => slide.plusSlides(1);

document.getElementById('slide-1').onclick = () => slide.currentSlide(1);
document.getElementById('slide-2').onclick = () => slide.currentSlide(2);
document.getElementById('slide-3').onclick = () => slide.currentSlide(3);