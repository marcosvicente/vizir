import Form  from './form';

export default class Validation extends Form {

  createTagError(error){
    var elem  = document.createElement('div');
    elem.className = 'errorInput';
    var text = document.createTextNode(error);
    elem.appendChild(text);
    return elem;
  }

  required(){
    if (this.valueInput().value == '') {
      return this.errorMensagemInput('input not is required');
    }
  }

  errorMensagemInput(error){
    let element = this.createTagError(error);
    let form = this.valueForm();
    let divError = document.getElementsByClassName('errorInput');


    for (const d of divError) {
      d.parentNode.removeChild(d);
    }

    for (const mensagem of form) {
      mensagem.appendChild(element);
    }

  }

  errroMensagem(error){
    let errorTag = document.getElementById('error');
    return errorTag.innerHTML = error;
  }


  main(){
    document.getElementById('btn-calculator').onclick = () => validation.required();
  }
}

var validation = new Validation();
validation.main();