import Validation from './validation';
// import Loader from './loader';

export default class Calculator extends Validation{
  orignValue(){
    var orign = document.getElementById('orign');
    return orign.value;
  }

  destinationValue(){
    var destination = document.getElementById('destination');
    return destination.value;
  }

  planValue(){
    var plan = document.getElementById('plan');
    return plan.value;
  }

  timeValue(){
    var time = document.getElementById('time');
    return time.value;
  }

  tablePrice(orign, destination){
    if (orign == 11 && destination == 16){
      return 1.90;
    }
    else if (orign == 16 && destination == 11){
      return 2.90;
    }
    else if (orign == 11 && destination == 17){
      return 1.70;
    }
    else if (orign == 17 && destination == 11){
      return 2.70;
    }
    else if (orign == 11 && destination == 18){
      return 0.90;
    }
    else if (orign == 18 && destination == 11){
      return 1.90;
    }
    else{
      this.errroMensagem(I18n.t('js.error.OrignOrDestinationNotExist'));
    }
  }

  percentagePlan(){
    let percentage = this.tablePrice(this.orignValue(), this.destinationValue()) * 0.10;
    let tablePrice = this.tablePrice(this.orignValue(), this.destinationValue()) + percentage;
    return tablePrice;
  }

  loaderCalculator(){
    const loader = document.getElementById('loader-calculator');
    return loader;
  }

  formatMoney(){
    const money = {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
      style: 'currency',
      currency: 'BRL'
    };
    return money;
  }

  calculatorPlan(){
    let result;
    // this is value  in minutes in the plan
    let minutes = [30, 60, 120];

    for (const m of minutes) {
      if (this.timeValue() < m && this.planValue() == m) {
        result = 0;
        break;
      }
      else if (this.timeValue() > m && this.planValue() == m) {
        result = (this.timeValue() - m) * this.percentagePlan();
        break;
      }
    }
    document.getElementById('result-with-plan').innerHTML = result.toLocaleString('pt-BR', this.formatMoney());
  }


  calculatorWithOutPlan(){
    let result = this.timeValue() * this.tablePrice(this.orignValue(), this.destinationValue());
    document.getElementById('result-without-plan').innerHTML = result.toLocaleString('pt-BR', this.formatMoney());
  }

  main(){
    let result = document.getElementById('result');
    if (this.orignValue() == this.destinationValue()) {
      this.errroMensagem(I18n.t('js.error.OrignAndDestinationNotBeEqual'));
    }
    else if(this.timeValue() == '' || this.timeValue() == 0){
      this.errroMensagem(I18n.t('js.error.OrignAndDestinationNotBe0'));
    }
    else{
      this.errroMensagem('');
      result.style.display = 'none';

      this.calculatorPlan();
      this.calculatorWithOutPlan();

      // show loader
      this.loaderCalculator().style.display = 'block';


      setTimeout(() => {
        // hide loader
        this.loaderCalculator().style.display = 'none';

        // show result
        result.style.display = 'block';
      }, 1000);
    }
  }
}

var calculator = new Calculator();
// function for run with the turbolinks
document.getElementById('btn-calculator').onclick = () => calculator.main();