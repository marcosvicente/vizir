class Menu{
  scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      document.getElementById('menu').style.background = '#379688';
      document.getElementById('logo').src = 'assets/logo-white.png';

    } else {
      document.getElementById('menu').style.background = '#0000001c';
      document.getElementById('logo').src = 'assets/logo.png';

    }
  }
}
var menu = new Menu();
window.onscroll = () => {menu.scrollFunction()};