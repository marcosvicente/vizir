const { environment } = require('@rails/webpacker')
const erb =  require('./loaders/erb')
const eslint =  require('./loaders/eslint')

environment.loaders.append('eslint', eslint)

environment.loaders.append('erb', erb)
module.exports = environment
