Rails.application.routes.draw do
  scope "(:locale)", locale: /en|nl/ do
    root to: 'home#index'
    get 'home/index'
  end
end
