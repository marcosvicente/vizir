source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.4.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.1'
# Use sqlite3 as the database for Active Record
gem 'sqlite3'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Pry is a powerful alternative to the standard IRB shell for Ruby.
gem 'pry-rails', :group => :development

# A library for generating fake data such as names, addresses, and phone numbers.
gem 'faker', :git => 'https://github.com/stympy/faker.git', :branch => 'master'

# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# flag-icon-css sass gem for use in Ruby/Rails projects.
gem 'flag-icons-rails'
# gem for use font awesome
gem 'font-awesome-sass', '~> 5.6.1'

# Normalize.css is an alternative to CSS resets
gem 'normalize-rails'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# # An asset pipeline wrapper for the TypeScript language
# gem 'typescript-rails'

# animate.css for rails.
gem "animate-rails"

# Using ES6 transformer
gem 'sprockets', '>= 3.0.0'
gem 'sprockets-es6'
gem 'webpacker', '~> 3.5'

# Using for work i18n with js
gem "i18n-js", ">= 3.0.0.rc11"



group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rspec-rails', '~> 3.7'
   # factory_bot is a fixtures replacement with a straightforward definition syntax, support for multiple build strategies
  gem "factory_bot_rails", "~> 4.0"
  # Shoulda Matchers provides RSpec- and Minitest-compatible one-liners that test common Rails functionality. These tests would otherwise be much longer, more complex, and error-prone.
  gem 'shoulda-matchers', '~> 3.1'
  # RSpec-specific analysis for your projects, as an extension to RuboCop.
  gem 'rubocop-rspec'
  # Extensions focused on enforcing Rails best practices and coding conventions
  gem 'rubocop-rails'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # A Ruby static code analyzer and formatter, based on the community Ruby style guide.
  gem 'rubocop', '~> 0.62.0', require: false

end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
